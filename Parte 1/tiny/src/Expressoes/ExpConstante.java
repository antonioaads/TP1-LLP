package Expressoes;

public class ExpConstante extends Expressao{
    private double valor;
    
    public ExpConstante(double valor){
        this.valor = valor;
    }
    public double avalia(){
        return (valor);
    }
}
