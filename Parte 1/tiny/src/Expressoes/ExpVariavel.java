package Expressoes;

import Interpretador.Variaveis; 

public class ExpVariavel extends Expressao{
    private char nomeVar;
    
    public ExpVariavel(char nomeVar){
        this.nomeVar = nomeVar;
    }
    public double avalia(){
        return (Variaveis.var[nomeVar-97]);
    }
}
