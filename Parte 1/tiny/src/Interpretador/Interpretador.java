package Interpretador;
 
import java.util.*;
import lp.*;
import Comandos.*;

public class Interpretador {
   private ArquivoFonte arq;
   private Vector comandos;   
   private String palavraAtual;
   		
   public Interpretador(String nome) {
      arq= new ArquivoFonte(nome);
      comandos= new Vector();
   }
   
   public void listaArquivo() {
      String palavra;
      
      do {
         palavra= arq.proximaPalavra();
         System.out.println ("Palavra: " + palavra);
      } while (!palavra.equals("EOF"));
   }
   
   public void leArquivo() {
      
      String comandoAtual;
      String text;          //Variável para leitura dos parâmetros String
      int linha= 0;
      int caracter;         //Variavel para conversão de caracter para posição do vetor
      
      do {
         comandoAtual= arq.proximaPalavra();
            
         if(comandoAtual.equals("endp")){
            trataComandoEndp(linha);
            linha++;
         }
         else if(comandoAtual.equals("writeln")){
            trataComandoWriteln(linha);
            linha++;
         }
         else if(comandoAtual.equals("writeStr")){
             arq.proximaPalavra(); //Leitura do Parênteses
             text = arq.proximaPalavra(); //Leitura do parâmetro
             trataComandoWriteStr(linha,text);
             linha++;
         }
         else if(comandoAtual.equals("writeVar")){
             arq.proximaPalavra(); //Leitura do Parênteses
             text = arq.proximaPalavra(); //Leitura do parâmetro
             trataComandoWriteVar(linha,text);
             linha++;
         }
         else if(comandoAtual.equals("read")){
             arq.proximaPalavra(); //Leitura do Parênteses
             text = arq.proximaPalavra(); //Leitura do parâmetro
             trataComandoRead(linha,text);
             linha++;
         }
         else if(comandoAtual.length()==1 && comandoAtual.charAt(0) >=97 && comandoAtual.charAt(0) <=122){
             //Detecção de atribuição
             arq.proximaPalavra(); //Leitura do :=
         }
                           		  
      } while (!comandoAtual.equals("endp"));
   }
   
   private void trataComandoEndp(int lin) {
      
      ComandoEndp c= new ComandoEndp(lin);
      comandos.addElement(c);
   }
   	   	
   private void trataComandoWriteln(int lin) {
      
      ComandoWriteln c= new ComandoWriteln(lin);
      comandos.addElement(c);
   } 

    private void trataComandoWriteStr(int lin, String tex) {
      
      ComandoWriteStr c= new ComandoWriteStr(lin,tex);
      comandos.addElement(c);
   } 
    
    private void trataComandoWriteVar(int lin, String txt) {
      
      ComandoWriteVar c= new ComandoWriteVar(lin,txt);
      comandos.addElement(c);
   } 
    
   private void trataComandoRead(int lin, String txt) {
      
      ComandoRead c= new ComandoRead(lin,txt);
      comandos.addElement(c);
   }
       
   public void executa() {
      
      Comando cmd;
      int pc= 0;
      do {
         cmd= (Comando) comandos.elementAt(pc);
         pc= cmd.executa();
      } while (pc != -1);
   }   
}
