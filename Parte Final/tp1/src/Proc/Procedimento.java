package Proc;
	
import Variavel.*;
import Comando.*;
import java.util.*;
	
	
public class Procedimento {
    
    String nome;                            // nome do procedimento  
    Vector comandos;    			// vetor de comandos 
    String [] parametros;			// parametros formais 
    String [] variaveisLocal;		// vari�veis local
    int tamanho_variaveis_locais;       //Quantidade de variaveis locais passada como parametro
    static Memoria global = new Memoria();// variaveis globais
		 
		 
    public Procedimento( String nome, String [] p ){
        this.nome= nome;
        parametros= p; 
        tamanho_variaveis_locais = 0;
    }
		
    public String getNome(){
        return nome;
    }
		
    public static void setVariaveisGlobal( String [] vG , int k){	
        // adiciona variáveis globais na memória global
        if( vG != null ){	
            for(int i = 0; i< k; i++ ){
                //String a = vG[i];
                //System.out.println(k);
                int posicao = vG[i].charAt(0) - 97;
                global.var_state[posicao] = true;
            }
        }
    }
		
    public void setVariaveisLocal( String [] vL , int k){
        this.tamanho_variaveis_locais = this.tamanho_variaveis_locais + k;
        this.variaveisLocal = vL;
    }
		
    public void setListaComandos( Vector c ){ 
        this.comandos = c;
    }
		
   public void executa(double [] argumentos, int k ) {
      Memoria local = new Memoria(); 				// variaveis local
      char var;
   	  
        // adiciona variáveis locais na memória local
        if( variaveisLocal != null ){
            //int i=0;
            /*while(i<10 && !" ".equals(variaveisLocal[i])){
                int posicao = variaveisLocal[i].charAt(0) - 97;
                local.var_state[posicao] = true;
                i++;
            }*/
            for(int i = 0; i<tamanho_variaveis_locais; i++){
                int posicao = variaveisLocal[i].charAt(0) - 97;
                local.var_state[posicao] = true;
            }
//          for(int i = 0; i < variaveisLocal.length; i++ ){
//              int posicao = variaveisLocal[i].charAt(0) - 97;
//              local.var_state[posicao] = true;
//          }
        }
                
        // associa argumentos a seus respectivos par�metros formais
         if( parametros != null ){
            /*int i = 0;
            while(i<10 && !" ".equals(parametros[i])){
                int posicao = parametros[i].charAt(0) - 97;
                local.var_state[posicao] = true;
                local.var[posicao] = argumentos[i];
                i++;
            }*/
             for(int i = 0; i<k; i++){
                int posicao = parametros[i].charAt(0) - 97;
                local.var_state[posicao] = true;
                local.var[posicao] = argumentos[i];
            }
        }
        
        //System.out.println("Chegou aqui proc");
          
        int pc= 0;
        do {       
            pc=  ((Comando) comandos.elementAt(pc)).executa(local, global);            
        } while (pc < comandos.size() );
    }
}
