package Expressoes;

import Interpretador.Variaveis; 

public class ExpBinaria extends Expressao{
    private String operador;
    private Object exp1;
    private Object exp2;
    
    public ExpBinaria(String operador, Object exp1, Object exp2){
        this.operador = operador;
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    public double avalia(){
        
        if(operador.equals("+")){
            return (((Expressao)exp1).avalia() + ((Expressao)exp2).avalia());
        }
        else if (operador.equals("-")){
            return (((Expressao)exp1).avalia() - ((Expressao)exp2).avalia());
        }
        else if (operador.equals("*")){
            return (((Expressao)exp1).avalia() * ((Expressao)exp2).avalia());
        }
        else if (operador.equals("/")){
            return (((Expressao)exp1).avalia() / ((Expressao)exp2).avalia());
        }
        else{
            System.out.println("Operador inválido");  
        }
        System.exit(0);
        return 0;
    }
}