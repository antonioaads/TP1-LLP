package Expressoes;

import Interpretador.Variaveis; 

public class ExpVariavel extends Expressao{
    private String var;
    
    public ExpVariavel(String var){
        this.var = var;
    }
    public double avalia(){
        int posicao = var.charAt(0); //conversão caracter para posição no vetor
        if (Variaveis.var_state[posicao-97] == false){
            System.out.println("Variável não declarada");
            System.exit(0);
        }
        return (Variaveis.var[posicao-97]);
    }
}
