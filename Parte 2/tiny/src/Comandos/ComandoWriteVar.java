package Comandos;

import Interpretador.*;

public class ComandoWriteVar extends Comando {
   
   String txt;
   int posicao; //Variável para converter caracter em posição de memória
   	
   public ComandoWriteVar(int lin, String texto) {
      linha= lin;
      txt = texto;
      
    posicao = txt.charAt(0);
    posicao -= 97;
   }
   
   public int executa() {
       if(Variaveis.var_state[posicao]==false){
           System.out.println("Variável não declarada"); //Posteriormente irei tratar como exceção
       }
       else{
           System.out.println(Variaveis.var[posicao]);
       }
      return linha+1;
   }
}