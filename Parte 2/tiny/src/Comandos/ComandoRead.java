package Comandos;

import java.util.*;
import Interpretador.*;

public class ComandoRead extends Comando {
   
   //BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
   Scanner ler = new Scanner(System.in);
   int posicao;
   String txt;
   	
   public ComandoRead(int lin, String texto) {
        linha= lin;
        txt = texto;
      
        posicao = txt.charAt(0);
        posicao -= 97;
   }
   
   public int executa() {
 
        float n;
        
      //try {
        n = ler.nextFloat(); // 3.1 entrada de dados (lendo um valor inteiro)
        Variaveis.var[posicao] = n; //Tratarems exceção posteriormente
        Variaveis.var_state[posicao] = true; //Variável para controle das declarações das variáveis
      /*}
      catch( Exception e) {
         System.out.println("ERRO: "+e);
      }*/
      return linha+1;
   }
}