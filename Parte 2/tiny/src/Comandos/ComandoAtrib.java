package Comandos;

import java.util.*;
import Interpretador.*;
import Expressoes.*;

public class ComandoAtrib extends Comando {
   Expressao exp;
   char var;
   int linha;
   	
   public ComandoAtrib(int linha, Expressao exp, char var) {
        this.exp = exp;
        this.var = var;
        this.linha = linha;
   }
   
    public int executa(){
        int posicao = var-97;   //conversão da variável para posição de memória
        Variaveis.var[posicao] = exp.avalia();
        Variaveis.var_state[posicao] = true; //Variável para controle das declarações das variáveis
        return linha+1;
    }
}